import 'package:daligo_customer_app/model/remain_amount_response.dart';

class RemainAmountResult {
  RemainAmountResponse data;
  bool isSuccess; // 성공 여부를 묻는 변수
  int code; // isSuccess 결과에 따른 코드 값을 담는 변수
  String msg; // isSuccess 결과에 따른 메세지를 담는 변수

  RemainAmountResult(this.data, this.isSuccess, this.code, this.msg);

  factory RemainAmountResult.fromJson(Map<String, dynamic> json) {
    return RemainAmountResult(
        RemainAmountResponse.fromJson(json['data']),
        json['isSuccess'] as bool,
        json['code'],
        json['msg']
    );
  }
}