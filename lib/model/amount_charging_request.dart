class AmountChargingRequest {
  String price;

  AmountChargingRequest(this.price);

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};

    data['price'] = price;

    return data;
  }
}