class RemainAmountResponse {
  num remainPrice;

  RemainAmountResponse(this.remainPrice);

  factory RemainAmountResponse.fromJson(Map<String, dynamic> json) {
    return RemainAmountResponse(
        json['remainPrice'],
    );
  }
}