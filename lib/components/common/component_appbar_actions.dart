import 'package:flutter/material.dart';

// PreferredSizeWidget 위젯 사이즈를 조정.
// 앱바같은 경우 기본으로 높이가 고정되어있으나 PreferredSizeWidget을 구현(implements) 함으로써
// 높이를 조절 할 수 있게 됨.
class ComponentAppbarActions extends StatefulWidget implements PreferredSizeWidget {
  const ComponentAppbarActions({
    super.key,
    required this.title,
    this.isUseActionBtn1 = false,
    this.action1Icon,
    this.action1Callback,
    this.isUseActionBtn2 = false,
    this.action2Icon,
    this.action2Callback,
  });

  final String title;
  final bool isUseActionBtn1;
  final IconData? action1Icon;
  final VoidCallback? action1Callback;
  final bool isUseActionBtn2;
  final IconData? action2Icon;
  final VoidCallback? action2Callback;

  @override
  State<ComponentAppbarActions> createState() => _ComponentAppbarActionsState();

  // 위젯의 높이를 45dp로 고정
  @override
  Size get preferredSize {
    return const Size.fromHeight(45);
  }
}

class _ComponentAppbarActionsState extends State<ComponentAppbarActions> {
  @override
  Widget build(BuildContext context) {
    return AppBar(
      centerTitle: false, // 타이틀(글자) 위치 가운데로 하겠니?
      title: Text(widget.title),
      elevation: 1.0, // 앱바 아래 그림자처럼 보이는 것
      actions: [
        if (widget.isUseActionBtn1)
          IconButton(onPressed: widget.action1Callback, icon: Icon(widget.action1Icon)),
        if (widget.isUseActionBtn2)
          IconButton(onPressed: widget.action2Callback, icon: Icon(widget.action2Icon)),
      ],
    );
  }
}
