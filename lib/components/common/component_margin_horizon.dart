import 'package:flutter/material.dart';
import 'package:daligo_customer_app/enums/enum_size.dart';

class ComponentMarginHorizon extends StatelessWidget {
  final EnumSize enumSize;

  const ComponentMarginHorizon({Key? key, this.enumSize = EnumSize.micro}) : super(key: key);

  double _getSize() {
    switch(enumSize) {
      case EnumSize.micro:
        return 3;
        break;
      case EnumSize.small:
        return 5;
        break;
      case EnumSize.mid:
        return 10;
        break;
      case EnumSize.big:
        return 15;
        break;
      case EnumSize.bigger:
        return 20;
    }
    return 3;
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: _getSize(),
    );
  }
}

