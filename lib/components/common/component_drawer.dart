import 'package:daligo_customer_app/model/my_info_result.dart';
import 'package:daligo_customer_app/pages/page_customer_service.dart';
import 'package:daligo_customer_app/pages/page_main.dart';
import 'package:daligo_customer_app/pages/page_my_information.dart';
import 'package:daligo_customer_app/pages/page_my_use_history.dart';
import 'package:daligo_customer_app/pages/page_charging_price.dart';
import 'package:daligo_customer_app/repository/repo_member.dart';
import 'package:flutter/material.dart';

class ComponentDrawer extends StatefulWidget {
  const ComponentDrawer({super.key});

  @override
  State<ComponentDrawer> createState() => _ComponentDrawerState();
}

class _ComponentDrawerState extends State<ComponentDrawer> {
  String name = "";
  String phoneNumber = "";

  Future<void> _getMyInfo() async {
    MyInfoResult result = await RepoMember().getMyInfo();

    setState(() {
      name = result.data.name;
      phoneNumber = result.data.phoneNumber;
    });
  }

  @override
  void initState() {
    super.initState();
    _getMyInfo();
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        children: [
          UserAccountsDrawerHeader(
            currentAccountPicture: const CircleAvatar(
                backgroundImage: AssetImage("assets/profile_img.jpg")
            ),
            accountName: Text(name),
            accountEmail: Text(phoneNumber),
            onDetailsPressed: () => Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (BuildContext context) => const PageMyInformation()), (route) => false),
            decoration: BoxDecoration(
              color: Colors.blue[200],
              borderRadius: const BorderRadius.only(
                bottomLeft: Radius.circular(10.0),
                bottomRight: Radius.circular(10.0),
              ),
            ),
          ),
          ListTile(
            leading: const Icon(Icons.home),
            iconColor: Colors.blue,
            focusColor: Colors.blue,
            title: const Text('홈'),
            onTap: () => Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (BuildContext context) => const PageMain()), (route) => false),
            trailing: const Icon(Icons.navigate_next),
          ),
          ListTile(
            leading: const Icon(Icons.account_circle),
            iconColor: Colors.blue,
            focusColor: Colors.blue,
            title: const Text('내 정보'),
            onTap: () => Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (BuildContext context) => const PageMyInformation()), (route) => false),
            trailing: const Icon(Icons.navigate_next),
          ),
          ListTile(
            leading: const Icon(Icons.money),
            iconColor: Colors.blue,
            focusColor: Colors.blue,
            title: const Text('금액충전'),
            onTap: () {Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (BuildContext context) => const PageChargingPrice()), (route) => false);},
            trailing: const Icon(Icons.navigate_next),
          ),
          ListTile(
            leading: const Icon(Icons.list_alt),
            iconColor: Colors.blue,
            focusColor: Colors.blue,
            title: const Text('이용기록'),
            onTap: () => Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (BuildContext context) => const PageMyUseHistory()), (route) => false),
            trailing: const Icon(Icons.navigate_next),
          ),
          ListTile(
            leading: const Icon(Icons.headset_mic),
            iconColor: Colors.blue,
            focusColor: Colors.blue,
            title: const Text('고객센터'),
            onTap: () => Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (BuildContext context) => const PageCustomerService()), (route) => false),
            trailing: const Icon(Icons.navigate_next),
          ),
        ],
      ),
    );
  }
}

