import 'package:daligo_customer_app/config/config_size.dart';
import 'package:flutter/material.dart';

class ComponentResponse extends StatefulWidget {
  const ComponentResponse({super.key, required this.title, required this.content});

  final String title;
  final String content;

  @override
  State<ComponentResponse> createState() => _ComponentResponseState();
}

class _ComponentResponseState extends State<ComponentResponse> {
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          widget.title,
          style: const TextStyle(
            fontSize: fontSizeBig,
          ),
        ),
        Text(
          widget.content,
          style: const TextStyle(
            fontSize: fontSizeBig,
          ),
        ),
      ],
    );
  }
}
