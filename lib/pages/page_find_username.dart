import 'package:bot_toast/bot_toast.dart';
import 'package:daligo_customer_app/components/common/component_appbar_actions.dart';
import 'package:daligo_customer_app/components/common/component_custom_loading.dart';
import 'package:daligo_customer_app/components/common/component_margin_vertical.dart';
import 'package:daligo_customer_app/components/common/component_notification.dart';
import 'package:daligo_customer_app/components/common/component_text_btn.dart';
import 'package:daligo_customer_app/config/config_color.dart';
import 'package:daligo_customer_app/config/config_form_validator.dart';
import 'package:daligo_customer_app/config/config_style.dart';
import 'package:daligo_customer_app/enums/enum_size.dart';
import 'package:daligo_customer_app/model/auth_check_request.dart';
import 'package:daligo_customer_app/model/auth_send_request.dart';
import 'package:daligo_customer_app/model/find_username_request.dart';
import 'package:daligo_customer_app/pages/page_check_username.dart';
import 'package:daligo_customer_app/repository/repo_member.dart';
import 'package:daligo_customer_app/repository/repo_phone_auth.dart';
import 'package:daligo_customer_app/styles/style_form_decoration.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';

class PageFindUsername extends StatefulWidget {
  const PageFindUsername({super.key});

  @override
  State<PageFindUsername> createState() => _PageFindUsernameState();
}

class _PageFindUsernameState extends State<PageFindUsername> {
  final _formKey = GlobalKey<FormBuilderState>();

  var maskPhoneNumber = MaskTextInputFormatter(
      mask: '###-####-####',
      filter: { "#": RegExp(r'[0-9]') },
      type: MaskAutoCompletionType.lazy
  );

  String _authSendBtnName = '인증번호 전송';
  bool _isAuthNumSend = false; // 인증번호 보냈냐??
  bool _isAuthNumSuccess = false; // 인증 완료 했냐??

  Future<void> _doAuthSend(AuthSendRequest request) async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoPhoneAuth().doSend(request).then((res) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: true,
        title: '인증번호 전송',
        subTitle: '인증번호가 ${request.phoneNumber} 으로 전송되었습니다.',
      ).call();

      setState(() {
        _authSendBtnName = '인증번호 재전송';
        _isAuthNumSend = res.isSuccess;
      });
    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: false,
        title: '인증번호 전송 실패',
        subTitle: '고객센터로 문의하세요.',
      ).call();
    });
  }

  Future<void> _doAuthCheck(AuthCheckRequest request) async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoPhoneAuth().doCheck(request).then((res) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: true,
        title: '인증번호 확인 완료',
        subTitle: '인증번호 확인이 완료되었습니다.',
      ).call();

      setState(() {
        _isAuthNumSuccess = res.isSuccess;
      });
    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: false,
        title: '인증번호 확인 실패',
        subTitle: '인증번호를 정확히 입력해주세요.',
      ).call();
    });
  }

  Future<void> _findUsername(FindUsernameRequest request) async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoMember().findUsername(request).then((res) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: true,
        title: '인증번호 확인 완료',
        subTitle: '인증번호 확인이 완료되었습니다.',
      ).call();

      Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(
              builder: (BuildContext context) => PageCheckUsername(username: res.data.username)),
              (route) => false);
    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: false,
        title: '인증번호 확인 실패',
        subTitle: '인증번호를 정확히 입력해주세요.',
      ).call();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const ComponentAppbarActions(
        title: '아이디 찾기',
      ),
      body: _buildBody(context),
    );
  }

  Widget _buildBody(BuildContext context) {
    return ListView(
      children: [
        const ComponentMarginVertical(),
        Container(
          padding: const EdgeInsets.only(
            bottom: 10,
            left: 20,
            right: 20,
          ),
          child: FormBuilder(
            key: _formKey,
            autovalidateMode: AutovalidateMode.disabled,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Container(
                  decoration: formBoxDecoration,
                  padding: bodyPaddingAll,
                  child: FormBuilderTextField(
                    name: 'name',
                    decoration: StyleFormDecoration().getInputDecoration('이름'),
                    maxLength: 20,
                    keyboardType: TextInputType.text,
                    validator: FormBuilderValidators.compose([
                      FormBuilderValidators.required(errorText: formErrorRequired),
                      FormBuilderValidators.minLength(2, errorText: formErrorMinLength(2)),
                      FormBuilderValidators.maxLength(20, errorText: formErrorMaxLength(20)),
                    ]),
                  ),
                ),
                const ComponentMarginVertical(),
                Container(
                  decoration: formBoxDecoration,
                  padding: bodyPaddingAll,
                  child: FormBuilderTextField(
                    name: 'phoneNumber',
                    inputFormatters: [maskPhoneNumber],
                    decoration: StyleFormDecoration().getInputDecoration('연락처'),
                    maxLength: 13,
                    keyboardType: TextInputType.text,
                    validator: FormBuilderValidators.compose([
                      FormBuilderValidators.required(errorText: formErrorRequired),
                      FormBuilderValidators.minLength(13, errorText: formErrorMinLength(13)),
                      FormBuilderValidators.maxLength(13, errorText: formErrorMaxLength(13)),
                    ]),
                    enabled: !_isAuthNumSuccess,
                  ),
                ),
                !_isAuthNumSuccess
                    ? Container(
                  child: ComponentTextBtn(
                    _authSendBtnName,
                        () {
                      String phoneNumberText = _formKey.currentState!.fields['phoneNumber']!.value;
                      if (phoneNumberText.length == 13) {
                        AuthSendRequest authSendRequest = AuthSendRequest(
                          phoneNumberText,
                        );
                        _doAuthSend(authSendRequest);
                      }
                    },
                    bgColor: colorSecondary,
                    borderColor: colorSecondary,
                  ),
                )
                    : Container(),
                const ComponentMarginVertical(),
                _isAuthNumSend && !_isAuthNumSuccess
                    ? Container(
                  decoration: formBoxDecoration,
                  padding: bodyPaddingAll,
                  child: FormBuilderTextField(
                    name: 'authNumber',
                    decoration:
                    StyleFormDecoration().getInputDecoration('인증번호'),
                    maxLength: 6,
                    keyboardType: TextInputType.text,
                    validator: FormBuilderValidators.compose([
                      FormBuilderValidators.required(
                          errorText: formErrorRequired),
                      FormBuilderValidators.minLength(6,
                          errorText: formErrorMinLength(6)),
                      FormBuilderValidators.maxLength(6,
                          errorText: formErrorMaxLength(6)),
                    ]),
                    enabled: _isAuthNumSend,
                  ),
                )
                    : Container(),
                _isAuthNumSend && !_isAuthNumSuccess
                    ? Container(
                  child: ComponentTextBtn(
                    '인증번호 확인',
                        () {
                      String phoneNumberText = _formKey
                          .currentState!.fields['phoneNumber']!.value;
                      String authNumberText =
                          _formKey.currentState!.fields['authNumber']!.value;

                      if (phoneNumberText.length == 13 &&
                          authNumberText.length == 6) {
                        AuthCheckRequest authCheckRequest =
                        AuthCheckRequest(
                          phoneNumberText,
                          authNumberText,
                        );
                        _doAuthCheck(authCheckRequest);
                      }
                    },
                    bgColor: colorSecondary,
                    borderColor: colorSecondary,
                  ),
                )
                    : Container(),
                const ComponentMarginVertical(
                  enumSize: EnumSize.big,
                ),
                (_isAuthNumSend && _isAuthNumSuccess)
                    ? Container(
                  child: ComponentTextBtn('다음', () {
                    if (_formKey.currentState!.saveAndValidate()) {
                      FindUsernameRequest request = FindUsernameRequest(
                        _formKey.currentState!.fields['name']!.value,
                        _formKey.currentState!.fields['phoneNumber']!.value,
                      );
                      _findUsername(request);
                    }
                  }),
                )
                    : Container(),
              ],
            ),
          ),
        ),
        const ComponentMarginVertical(
          enumSize: EnumSize.small,
        )
      ],
    );
  }
}