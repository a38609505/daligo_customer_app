import 'package:daligo_customer_app/components/common/component_appbar_actions.dart';
import 'package:daligo_customer_app/components/common/component_drawer.dart';
import 'package:daligo_customer_app/components/common/component_margin_vertical.dart';
import 'package:daligo_customer_app/config/config_color.dart';
import 'package:daligo_customer_app/config/config_size.dart';
import 'package:daligo_customer_app/config/config_style.dart';
import 'package:daligo_customer_app/enums/enum_size.dart';
import 'package:daligo_customer_app/model/my_history_result.dart';
import 'package:daligo_customer_app/repository/repo_kick_board_history.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class PageMyUseHistoryDetail extends StatefulWidget {
  const PageMyUseHistoryDetail(
      {super.key, required this.id});

  final int id;

  @override
  State<PageMyUseHistoryDetail> createState() => _PageMyUseHistoryDetailState();
}

class _PageMyUseHistoryDetailState extends State<PageMyUseHistoryDetail> {
  String kickBoard = "";
  String dateStart = "";
  String dateEnd = "";
  String timeUse = "";
  num basePrice = 0;
  num perMinPrice = 0;
  num resultPrice = 0;

  var maskNumFormatter = NumberFormat('###,###,###,###');

  Future<void> _getHistory() async {
    MyHistoryResult result = await RepoKickBoardHistory().getHistory(widget.id);

    setState(() {
      kickBoard = result.data.kickBoard;
      dateStart = result.data.dateStart;
      dateEnd = result.data.dateEnd;
      timeUse = result.data.timeUse;
      basePrice = result.data.basePrice;
      perMinPrice = result.data.perMinPrice;
      resultPrice = result.data.resultPrice;
    });
  }

  @override
  void initState() {
    super.initState();
    _getHistory();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const ComponentAppbarActions(
        title: "나의 이용기록",
      ),
      body: _buildBody(),
      drawer: const ComponentDrawer(),
    );
  }

  Widget _buildBody() {
    return ListView(
      children: [
        Container(
          padding: const EdgeInsets.all(20),
          margin: const EdgeInsets.only(bottom: 10),
          child: Container(
            padding: const EdgeInsets.all(15),
            decoration: BoxDecoration(
              color: Colors.white,
              border: Border.all(color: colorLightGray),
              borderRadius: BorderRadius.circular(buttonRadius),
            ),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                const Align(
                  alignment: Alignment.centerLeft,
                  child: Text(
                    "이용 내역",
                    style: TextStyle(
                      fontSize: fontSizeBig,
                      fontWeight: FontWeight.w700,
                    ),
                  ),
                ),
                const ComponentMarginVertical(),
                const Divider(height: 1),
                const ComponentMarginVertical(),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    const Text(
                      "달리고 번호",
                      style: TextStyle(
                        fontSize: fontSizeMid,
                      ),
                    ),
                    Text(
                      kickBoard,
                      style: const TextStyle(
                        fontSize: fontSizeMid,
                      ),
                    ),
                  ],
                ),
                const ComponentMarginVertical(),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    const Text(
                      "탑승 일시",
                      style: TextStyle(
                        fontSize: fontSizeMid,
                      ),
                    ),
                    Text(
                      dateStart,
                      style: const TextStyle(
                        fontSize: fontSizeMid,
                      ),
                    ),
                  ],
                ),
                const ComponentMarginVertical(),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    const Text(
                      "하차 일시",
                      style: TextStyle(
                        fontSize: fontSizeMid,
                      ),
                    ),
                    Text(
                      dateEnd,
                      style: const TextStyle(
                        fontSize: fontSizeMid,
                      ),
                    ),
                  ],
                ),
                const ComponentMarginVertical(),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    const Text(
                      "적립 포인트",
                      style: TextStyle(
                        fontSize: fontSizeMid,
                      ),
                    ),
                    Text(
                      "${maskNumFormatter.format(resultPrice * 0.01)} 포인트",
                      style: const TextStyle(
                        fontSize: fontSizeMid,
                      ),
                    ),
                  ],
                ),
                const ComponentMarginVertical(enumSize: EnumSize.big),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    const Text(
                      "최종 결제 요금",
                      style: TextStyle(
                          fontSize: fontSizeBig, fontWeight: FontWeight.w700),
                    ),
                    Text(
                      "${maskNumFormatter.format(resultPrice)}원",
                      style: const TextStyle(
                          fontSize: fontSizeBig, fontWeight: FontWeight.w700),
                    ),
                  ],
                ),
                const ComponentMarginVertical(),
                const Divider(height: 1),
                const ComponentMarginVertical(),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    const Text(
                      "기본 요금",
                      style: TextStyle(
                        fontSize: fontSizeMid,
                      ),
                    ),
                    Text(
                      "${maskNumFormatter.format(basePrice)}원",
                      style: const TextStyle(
                        fontSize: fontSizeMid,
                      ),
                    ),
                  ],
                ),
                const ComponentMarginVertical(),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    const Text(
                      "분당 추가 요금",
                      style: TextStyle(
                        fontSize: fontSizeMid,
                      ),
                    ),
                    Text(
                      "${maskNumFormatter.format(perMinPrice)}원",
                      style: const TextStyle(
                        fontSize: fontSizeMid,
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
