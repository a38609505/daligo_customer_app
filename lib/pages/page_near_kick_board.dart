import 'dart:async';

import 'package:bot_toast/bot_toast.dart';
import 'package:daligo_customer_app/pages/page_qr_reader.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:daligo_customer_app/components/common/component_appbar_logo.dart';
import 'package:daligo_customer_app/components/common/component_custom_loading.dart';
import 'package:daligo_customer_app/components/common/component_no_contents.dart';
import 'package:daligo_customer_app/components/common/component_notification.dart';
import 'package:daligo_customer_app/functions/geo_check.dart';
import 'package:daligo_customer_app/repository/repo_kick_board.dart';
import 'package:geolocator/geolocator.dart';

class PageNearKickBoard extends StatefulWidget {
  const PageNearKickBoard({super.key});

  @override
  State<PageNearKickBoard> createState() => _PageNearKickBoardState();
}

class _PageNearKickBoardState extends State<PageNearKickBoard> {
  final Completer<GoogleMapController> _controller = Completer<GoogleMapController>();

  Position? _currentPosition;

  CameraPosition? _currentCamera;
  final _markers = <Marker>{};

  void _startRoutine() async {
    await _getPosition().then((res) {
      _loadItems(res.latitude, res.longitude);
    });
  }

  Future<void> _loadItems(double posX, double posY) async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(
        cancelFunc: cancelFunc,
      );
    });

    await RepoKickBoard().getList(posX, posY).then((res) => {
      BotToast.closeAllLoading(),
      setState(() {
        // _markers.addAll(
        //     res.list!.map((e) => Marker(
        //       markerId: MarkerId(e.modelName),
        //       infoWindow: InfoWindow(title: '[${e.kickBoardStatusName}] ${e.modelName} (${e.priceBasisName})'),
        //       position: LatLng(e.posX as double, e.posY as double),
        //     ))
        // );

      })
    }).catchError((err) => {
      BotToast.closeAllLoading(),
    });
  }



  Future<Position> _getPosition() async {
    try {
      Position position = await GeoCheck.determinePosition();

      setState(() {
        _currentPosition = position;
        _currentCamera = CameraPosition(
          target: LatLng(
            _currentPosition!.latitude,
            _currentPosition!.longitude,
          ),
          zoom: 17,
        );
      });

      return position;

    } catch(e) {
      ComponentNotification(
        success: false,
        title: '근처 킥보드 로딩 실패',
        subTitle: e.toString(),
      ).call();

      return Future.error('킥보드 로딩 실패');
    }
  }

  @override
  void initState() {
    super.initState();
    _startRoutine();
    _markers.add(Marker(markerId: MarkerId("1"),
        draggable: true,
        position: LatLng(37.300086, 126.838205)
    )
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ComponentAppbarLogo('logo.png', () {}),
      body: _buildBody(),
    );
  }

  Widget _buildBody() {
    if (_currentPosition != null) {
      return Stack(
        children: [
          GoogleMap(
            myLocationEnabled: true,
            mapType: MapType.normal,
            markers: _markers,
            initialCameraPosition: _currentCamera!,
            onMapCreated: (GoogleMapController controller) {
              _controller.complete(controller);
            },
          ),
          Positioned(
            left: MediaQuery.of(context).size.width / 2,
            bottom: 0,
            child: FloatingActionButton(
              onPressed: () {Navigator.of(context).push(MaterialPageRoute(builder: (BuildContext context) => PageQrReader(posX: _currentPosition!.latitude, posY: _currentPosition!.longitude)));},
              child: const Icon(Icons.qr_code_2_rounded),
            ),
          )
        ],
      );
    } else {
      return SizedBox(
        height: MediaQuery.of(context).size.height - 30 - 50 - 50 - 70,
        child: const ComponentNoContents(
          icon: Icons.gps_fixed,
          msg: '위치정보가 있어야만 사용가능합니다.',
        ),
      );
    }
  }
}
